const { src, dest, watch, series } = require('gulp');
const rename = require('gulp-rename');
const gulpPlumber = require('gulp-plumber');
const GulpPostCss = require('gulp-postcss');
const mq = require('postcss-sort-media-queries');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sass = require('gulp-sass')(require('sass'));

const sassPath = './website/assets/scss/**/*.scss';

const sassTask = () => {
  return src(sassPath, { sourcemaps: true })
    .pipe(gulpPlumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(GulpPostCss([mq(), autoprefixer(), cssnano()]))
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('./website/assets/css', { sourcemaps: '.' }));
};

const watchTask = (done) => {
  watch(sassPath, sassTask);
  done();
};

exports.default = series(sassTask, watchTask);
