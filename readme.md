## はじめに
---
基となるフォルダ名をiw27に変更します。  

node_modulesをインストールします。
```bash
npm install
```

## 開発を始めるとき
---
次のコマンドを動かしてから開発を始めます。  
```bash
npm start 
```
止めるときは `ctrl(command) + c`で停止します。