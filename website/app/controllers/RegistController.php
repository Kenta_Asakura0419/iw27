<?php
require_once 'app/models/User.php';

class RegistController
{
  function index()
  {
    $user = new User();

    //前回入力内容の読み込み
    $user->value = Session::load('posts');
    //エラー読み込み
    $errors = Session::load('errors');

    $template = 'app/views/regist/index.view.php';
    include 'app/views/layouts/app.view.php';

    Session::clear('errors');
  }


  // 確認画面
  function confirm()
  {
    // POSTで送られてきているか
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $user = new User();

      $posts = $user->check($_POST);
      $errors = $user->validateRegist($posts);
      // メールアドレス重複確認
      if ($user->findByEmail($posts['email'])) {
        $errors['email'] = 'メールアドレスがすでに登録されています';
      }

      Session::save('posts', $posts);
      Session::save('errors', $errors);

      $posts = Session::load('posts');

      if (!$errors) {
        Session::clear('errors');

        $template = 'app/views/regist/confirm.view.php';
        include 'app/views/layouts/app.view.php';
      } else {
        URL::redirect('index.php');
      }
    } else {
      URL::redirect('index.php');
    }
  }


  // 登録処理 & 完了画面
  function result()
  {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $user = new User();

      $posts = Session::load('posts');

      $user->insert($posts);

      Session::clear('posts');

      $template = 'app/views/regist/result.view.php';
      include 'app/views/layouts/app.view.php';
    } else {
      URL::redirect('index.php');
    }
  }
}
