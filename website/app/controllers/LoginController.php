<?php
require_once 'app/models/User.php';

class LoginController
{
  function index()
  {
    // ログイン失敗メッセージ
    $error = Session::load('loginError');

    $template = 'app/views/login/index.view.php';
    include 'app/views/layouts/app.view.php';

    // ログイン失敗メッセージの削除
    Session::clear('loginError');
  }


  // ログイン
  function auth()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $posts = $_POST;
      $user = new User();

      // ログイン処理
      $isAuth = $user->auth($posts['email'], $posts['password']);
      if ($isAuth) {
        $authenicatedUser = Session::load('authUser');

        if ($authenicatedUser['name'] === 'admin') {
          URL::redirect(PAGES . 'admin/'); // = URL::redirect("/iw27/website/admin/")
        }

        URL::redirect(PAGES . 'mypage/'); // = URL::redirect("/iw27/website/")
      } else {
        Session::save('loginError', 'ログインに失敗しました。');
        URL::redirect('index.php');
      }
    }
  }
}
