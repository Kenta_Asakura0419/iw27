<?php
require_once 'app/models/User.php';
require_once 'app/models/Item.php';
require_once 'app/models/Cart.php';

class MypageController
{
  function index()
  {
    $user = new User();

    $user->value = Session::load('authUser');

    $item = new Item();
    $cart = new Cart();

    $items = $item->allItems(3);

    // 閲覧履歴
    if (Session::has('history')) {
      $histories = Session::load('history');
      $exploredItems = [];
      foreach ($histories as $key => $value) {
        $item2 = new Item();
        $exploredItem = $item2->getItemById($value);
        array_push($exploredItems, $exploredItem);
      }
    }

    // 購入履歴
    $currentBuy = $cart->findBoughtItem($user->value["id"]);

    $template = 'app/views/mypage/index.view.php';
    include 'app/views/layouts/app.view.php';
  }

  // 会員情報変更画面
  function edit()
  {
    $user = new User();

    $user->value = Session::load('authUser');
    $editError = Session::load('editError');
    $errors = Session::load('errors');

    Session::clear('editError');
    Session::clear('errors');

    $template = 'app/views/mypage/user_edit/index.view.php';
    include 'app/views/layouts/app.view.php';
  }


  function confirm()
  {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $user = new User();
      $user->value = Session::load('authUser');


      $posts = $user->check($_POST);
      $errors = $user->validateEdit($posts);

      Session::save('errors', $errors);

      if (!$errors) {
        Session::clear('errors');

        Session::save('posts', $posts);

        $template = 'app/views/mypage/user_edit/confirm.view.php';
        include 'app/views/layouts/app.view.php';
      } else {
        Session::save('editError', '更新に失敗しました。');
        URL::redirect('index.php');
      }
    } else {
      URL::redirect('index.php');
    }
  }


  // 更新処理 & 完了画面
  function result()
  {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $user = new User();

      $posts = Session::load('posts');

      $user->updateUser($posts);

      Session::clear('authUser');
      Session::clear('posts');

      $template = 'app/views/mypage/user_edit/result.view.php';
      include 'app/views/layouts/app.view.php';
    } else {
      URL::redirect('index.php');
    }
  }


  // ユーザー削除確認
  function deleteConfirm()
  {
    $template = 'app/views/mypage/user_delete/deleteConfirm.view.php';
    include 'app/views/layouts/app.view.php';
  }

  // ユーザー削除
  function delete()
  {
    $user = new User();
    $user->deleteUser(Session::load('authUser'));

    Session::clear('authUser');

    $template = 'app/views/mypage/user_delete/result.view.php';
    include 'app/views/layouts/app.view.php';
  }
}
