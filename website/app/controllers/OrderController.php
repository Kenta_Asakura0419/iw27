<?php

require_once "app/models/Item.php";
require_once "app/models/Cart.php";

/**
 * class OrderController
 */

class OrderController
{
  public function index()
  {
    $authUser = Session::load('authUser');
    $item = new Item();
    $cart = new Cart();

    $user_id = $authUser["id"];
    $cart_id = null;

    if (!isset($authUser)) {
      Session::save('loginError', 'カート機能を使うにはログインが必要です。');
      URL::redirect(URL::route("login"));
    }

    if (isset($_GET) && isset($_GET["c"])) {
      $cart_id_hash = $_GET["c"];
      $cart_id = openssl_decrypt($cart_id_hash, 'AES-256-CBC', 'cart_id_special_key');

      // 削除フラグがある場合->削除
      if (isset($_GET["item_id"]) && isset($_GET["delete"])) {
        if ($_GET["delete"] === "true") {
          if ($cart->findByCartId($cart_id)) {
            $cart->delete($user_id, $cart_id, $_GET["item_id"]);
          } else {
            URL::redirect(URL::route("cart/"));
          }
        }
      }
    } else {
      // カートに何もない状態でここを訪れた場合
      URL::redirect(URL::route(""));
    }

    $stockItems = $cart->findStockCart($user_id, $cart_id);
    $template = 'app/views/order/confirmation.view.php';
    include 'app/views/layouts/app.view.php';
  }

  public function doneView()
  {
    $authUser = Session::load('authUser');
    $item = new Item();
    $cart = new Cart();

    $user_id = $authUser["id"];
    $cart_id = null;

    if (!isset($authUser)) {
      Session::save('loginError', 'カート機能を使うにはログインが必要です。');
      URL::redirect(URL::route("login"));
    }

    if (isset($_POST) && isset($_POST["c"])) {
      $cart_id_hash = $_POST["c"];
      $cart_id = openssl_decrypt($cart_id_hash, 'AES-256-CBC', 'cart_id_special_key');

      $cart->buy($user_id, $cart_id);
      $buy_items = $cart->findNowBoughtItem($user_id, $cart_id);
    } else {
      URL::redirect(URL::route(""));
    }

    $random_num = random_int(0, $item->count());
    $random_recommend_items = $item->allItems(4, $random_num);
    $template = 'app/views/order/completion.view.php';

    include 'app/views/layouts/app.view.php';
  }
}
