<?php
require_once 'app/models/User.php';

class LogoutController
{
  function index()
  {
    $user = new User();
    $user->logout();
  }
}
