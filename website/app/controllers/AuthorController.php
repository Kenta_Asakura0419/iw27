<?php

require_once "app/models/Item.php";
require_once "app/models/Anime.php";

/**
 * class AuthorController
 */

class AuthorController
{
  public function index()
  {
    $authUser = Session::load('authUser');
    $anime = new Anime();

    if ($author_id = filter_input(INPUT_GET, 'author_id')) {
      $item = new Item();
      if ($data = $anime->fetchOneAuthor($author_id)) {
        $goods = $item->getItemsByAuthor($author_id);
      } else {
        // author_idが存在しない場合
        header("HTTP/1.1 404 Not Found");
        $template = 'app/views/notfound.view.php';
        include 'app/views/layouts/app.view.php';
        die();
      }
      $template = 'app/views/authors/author.view.php';
    } else {
      // author_idが指定されていない場合
      $authors = $anime->fetchAllAuthors();
      $template = 'app/views/authors/index.view.php';
    }

    include 'app/views/layouts/app.view.php';
  }
}
