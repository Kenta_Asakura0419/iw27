<?php

require_once "app/models/Item.php";
require_once "app/models/Category.php";

/**
 * class CategoryController
 */

class CategoryController
{
  private $limit_num = 28;

  public function index()
  {
    $authUser = Session::load('authUser');
    $item = new Item();
    $cat = new Category();
    $category_id = filter_input(INPUT_GET, 'category_id');

    $items = [];
    $category = $cat->getCategory($category_id);

    if (!$category) {
      header("HTTP/1.1 404 Not Found");
      $template = 'app/views/notfound.view.php';
      include 'app/views/layouts/app.view.php';
      die();
    }

    $count = $item->count($category_id);

    $current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;

    $limit = $this->limit_num;
    $offset = ($current_page - 1) * $limit;
    $items = $item->getItemByCategory($category_id, $limit, $offset);

    $params = "&category_id={$category_id}";
    $paginate = $item->paginate($count, $current_page, $limit, 3);
    extract($paginate);

    $template = 'app/views/categories/index.view.php';
    include 'app/views/layouts/app.view.php';
  }
}
