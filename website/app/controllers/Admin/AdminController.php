<?php
class AdminController
{
  public function index()
  {
    $authUser = Session::load('authUser');
    if ($authUser && $authUser["name"] === "admin") {
      $template = 'app/views/admin/index.view.php';
      include 'app/views/layouts/app.view.php';
    } else {
      URL::redirect(PAGES);
    }
  }
}
