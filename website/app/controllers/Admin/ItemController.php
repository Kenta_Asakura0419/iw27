<?php
require_once 'app/models/Item.php';

class ItemController
{
  function index()
  {
    $authUser = Session::load('authUser');
    if ($authUser && $authUser["name"] === "admin") {
      $item = new item();

      $count = $item->count();

      $current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;

      $limit = 50;
      $offset = ($current_page - 1) * $limit;

      $items = $item->allItems($limit, $offset);

      $paginate = $item->paginate($count, $current_page, $limit, 5);
      extract($paginate);

      $template = 'app/views/admin/admin_item/index.view.php';
      include 'app/views/layouts/app.view.php';
    } else {
      URL::redirect(PAGES);
    }
  }

  function add()
  {
    $authUser = Session::load('authUser');
    if ($authUser && $authUser["name"] === "admin") {
      $item = new Item();

      if (Session::load('item_add_success')) {
        $successMessage = Session::load('item_add_success');
        Session::clear('item_add_success');
      };

      // カテゴリーの表示
      $categories = $item->categories();
      $item->value = array('category_id' => 1); // 初期値: 1
      Session::load('item_posts') && $item->value = Session::load('item_posts');
      $errors = Session::load('item_errors');

      //View
      $template = 'app/views/admin/admin_item/add.view.php';
      include 'app/views/layouts/app.view.php';

      Session::clear('item_errors');
      Session::clear('item_add_success');
    } else {
      URL::redirect("index.php");
    }
  }

  function result()
  {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $item = new Item();
      $imgPath = '../../../assets/images/';

      $posts = $item->check($_POST);
      // $posts['anime_title'] = str_replace(' ', ',', $posts['anime_title']);
      $files = $_FILES['item_images'];

      // 入力内容のバリデーション
      $errors = $item->validate($posts, $files);

      Session::save('item_posts', $posts);
      Session::save('item_errors', $errors);

      if (!$errors) {
        Session::clear('item_errors');

        $item->insert($posts, $files);
        $item->moveUploadedFile($files, $imgPath);
        Session::save('item_add_success', "成功:{$posts['item_name']}");
        Session::clear('item_posts');
        URL::redirect('add.php');
      } else {
        URL::redirect('add.php');
      }
    } else {
      URL::redirect('index.php');
    }
  }

  function update()
  {
    $item = new Item();
    $categories = $item->categories();
    if (Session::load('item_posts')) {
      $item->value = Session::load('item_posts');
      Session::clear('item_posts');
    } else {
      $item->value = $item->getItemById($_GET['item_id']);
    }
    $errors = Session::load('item_errors');

    $template = 'app/views/admin/admin_item/update.view.php';
    include 'app/views/layouts/app.view.php';
  }

  function update_result()
  {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $item = new Item();
      $imgPath = '../../assets/images/';

      $posts = $item->check($_POST);
      // $posts['anime_title'] = str_replace(' ', ',', $posts['anime_title']);
      $files = $_FILES['item_images'];

      $errors = $item->validate($posts, $files);

      Session::save('item_posts', $posts);
      Session::save('item_errors', $errors);

      if (!$errors) {
        Session::clear('item_errors');

        $item->update($posts, $files);
        $item->moveUploadedFile($files, $imgPath);
        Session::clear('item_posts');
        URL::redirect('index.php');
      } else {
        Session::clear('item_posts', $posts);
        Session::save('item_posts', $posts);
        URL::redirect("update.php?item_id={$posts['item_id']}");
      }
    } else {
      URL::redirect('index.php');
    }
  }

  function delete()
  {
    if (isset($_GET['item_id'])) {
      $item = new Item();

      $item->delete($_GET['item_id']);

      URL::redirect('index.php');
    } else {
      URL::redirect('index.php');
    }
  }
}
