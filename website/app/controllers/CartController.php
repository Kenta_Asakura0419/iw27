<?php
ini_set("display_errors", 0);

require_once "app/models/Item.php";
require_once "app/models/Cart.php";

/**
 * class CartController
 */

class CartController
{
  public function index()
  {
    $authUser = Session::load('authUser');

    if (!isset($authUser)) {
      Session::save('loginError', 'カート機能を使うにはログインが必要です。');
      URL::redirect(URL::route("login"));
    }

    $item = new Item();
    $cart = new Cart();

    $user_id = $authUser["id"];
    $cart_id = null;



    if (isset($_COOKIE["cart_id"]) && $cart->findByCartId($_COOKIE["cart_id"])) {
      // カートIDがすでに発行されている場合
      $cart_id = $_COOKIE["cart_id"];


      // 量とIDが送られてきた場合
      if (isset($_GET["quantity"]) && isset($_GET["item_id"])) {
        $quantity = $_GET["quantity"];
        $item_id = $_GET["item_id"];

        if ($existItem = $cart->findItemAlreadyInCart($user_id, $item_id, $cart_id)) {
          // すでに商品がカートに存在していた場合->更新
          $cart->update($user_id, $cart_id, $item_id, $quantity, $existItem["item_quantity"]);
          URL::redirect(URL::route("cart/"));
        } else {
          // すでに商品がカートに存在していなかった場合->新規追加
          $cart->insert($user_id, $item_id, $quantity, $cart_id);
        }
      }
    } else {
      // カートIDが発行されていない場合
      $cart_id = $cart->generateCartId();
      // 10年保持
      setcookie("cart_id", $cart_id, time() + 10 * 365 * 24 * 60 * 60);

      // 商品IDと量が設定されていたとき->カートに新規追加
      if (isset($_GET["quantity"]) && isset($_GET["item_id"])) {
        $quantity = $_GET["quantity"];
        $item_id = $_GET["item_id"];
        $cart->insert($user_id, $item_id, $quantity, $cart_id);
      }
    }

    // 更新フラグが送られてきた場合
    if (isset($_GET["update_quantity"]) && isset($_GET["item_id"]) && $_GET["update"]) {
      if ($_GET["update"] === "true") {
        $update_quantity = $_GET["update_quantity"];
        $item_id = $_GET["item_id"];
        $existItem = $cart->findItemAlreadyInCart($user_id, $item_id, $cart_id);
        $cart->update($user_id, $cart_id, $item_id, $update_quantity);
      }
    }

    // 削除フラグがある場合->削除
    if (isset($_GET["item_id"]) && isset($_GET["delete"])) {
      if ($_GET["delete"] === "true") {
        $cart->delete($user_id, $cart_id, $_GET["item_id"]);
      }
    }

    $cart_id_hash = openssl_encrypt($cart_id, 'AES-256-CBC', 'cart_id_special_key');
    $stockItems = $cart->findStockCart($user_id, $cart_id);

    $template = 'app/views/cart/cart.view.php';

    include 'app/views/layouts/app.view.php';
  }
}
