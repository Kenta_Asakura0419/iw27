<?php
require_once 'app/models/User.php';
require_once 'app/models/Anime.php';
require_once 'app/models/Item.php';

class HomeController
{
  function index()
  {
    $user = new User();
    $user->value = Session::load('authUser');

    $anime = new Anime();
    $animes = $anime->fetchAllAnimes();

    $item = new Item();
    $categories = $item->categories();
    $currentItems = $item->allItems(8);
    $books = (array)$item->getItemByCategory(11, 200);

    $count = count($books) >= 8 ? 8 : count($books);
    $keys = array_rand($books, $count);
    foreach ($keys as $key) {
      $randomBooks[] = $books[$key];
    }

    // 検索されたら
    if (isset($_GET["search_query"])) {
      $categories = $item->categories();

      $fulldata =  $item->getItemsBySearch(htmlspecialchars($_GET["search_query"], ENT_QUOTES));
      $current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
      $limit = 12;
      $offset = ($current_page - 1) * $limit;
      $data = $item->getItemsBySearch(htmlspecialchars($_GET["search_query"], ENT_QUOTES), $limit, $offset);

      $count = count($fulldata);

      $params = "&search_query=" . htmlspecialchars($_GET["search_query"], ENT_QUOTES);
      $paginate = $item->paginate($count, $current_page, $limit, 3);
      extract($paginate);

      $template = 'app/views/search.view.php';
    } else {
      $template = 'app/views/index.view.php';
    }

    // ベースになるレイアウト
    include 'app/views/layouts/app.view.php';
  }
}
