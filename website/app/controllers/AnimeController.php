<?php

require_once "app/models/Item.php";
require_once "app/models/Anime.php";

/**
 * class AnimeController
 */

class AnimeController
{
  public function index()
  {
    $authUser = Session::load('authUser');
    $anime = new Anime();

    if ($anime_id = filter_input(INPUT_GET, 'anime_id')) {
      $item = new Item();
      $data = [];
      if ($data = $anime->fetchOneAnime($anime_id)) {
        $categories = $item->categories();
        $books = $item->sortItems($data->anime_title, 11);
      } else {
        header("HTTP/1.1 404 Not Found");
        $template = 'app/views/notfound.view.php';
        include 'app/views/layouts/app.view.php';
        die();
      }
      $template = 'app/views/animes/anime.view.php';
    } else {

      $animes = $anime->fetchAllAnimes();

      $template = 'app/views/animes/index.view.php';
    }

    include 'app/views/layouts/app.view.php';
  }
}
