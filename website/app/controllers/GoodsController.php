<?php

require_once "app/models/Item.php";

/**
 * class GoodsController
 */

class GoodsController
{
  private $limit_num = 28;


  public function index()
  {
    $hist_limit = 4;

    $authUser = Session::load('authUser');

    if ($goods_id = filter_input(INPUT_GET, 'goods_id')) {
      $item = new Item();
      $data = [];
      if ($data = $item->getItemById($goods_id)) {
        $keyVisual = $item->divideImages($data["item_image"])[0];
        $subVisual = $item->divideImages($data["item_image"])[1];

        if (Session::has('history')) {
          $histories = Session::load('history');
          if (!in_array($goods_id, $histories, true)) {
            if (count($histories) >= $hist_limit) {
              array_pop($histories);
            }
            array_unshift($histories, $goods_id);
            Session::save('history', $histories);
          }
        } else {
          Session::save('history', [$goods_id]);
        }
      } else {
        header("HTTP/1.1 404 Not Found");
        $template = 'app/views/notfound.view.php';
        include 'app/views/layouts/app.view.php';
        die();
      }
      $template = 'app/views/goods/goods.view.php';
    } else {
      $item = new item();
      $count = $item->count();
      $current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
      $limit = $this->limit_num;
      $offset = ($current_page - 1) * $limit;
      $items = $item->allItems($limit, $offset);
      $paginate = $item->paginate($count, $current_page, $limit, 3);
      extract($paginate);
      $template = 'app/views/goods/index.view.php';
    }
    include 'app/views/layouts/app.view.php';
  }
}
