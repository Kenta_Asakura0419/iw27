<?php
require_once "Model.php";

require_once "Item.php";

/**
 * class Cart
 */

// --- table_sample --- //
// id
// user_id
// item_id
// item_quantity
// cart_id
// ------------------ //

class Cart extends Model
{
  public function generateCartId()
  {
    $cart_id = null;
    while (true) {
      $cart_id = mt_rand();
      if ($this->findByCartId($cart_id)) {
        $cart_id = mt_rand();
        continue;
      } else {
        break;
      }
    }

    return $cart_id;
  }

  public function findStockCart($user_id, $cart_id)
  {
    $sql = "SELECT carts.item_id, carts.item_quantity, items.item_image, items.item_name, items.item_price, items.anime_title";
    $sql .= " FROM carts INNER JOIN items ON carts.item_id = items.item_id";
    $sql .= " WHERE user_id = :user_id AND cart_id = :cart_id AND buy_flg = 0";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':user_id', $user_id);
    $stmt->bindValue(':cart_id', $cart_id);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
  }

  public function findByCartId($cart_id)
  {
    $sql = "SELECT cart_id FROM carts WHERE cart_id = :cart_id AND buy_flg = 0";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':cart_id', $cart_id);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function findItemAlreadyInCart($user_id, $item_id, $cart_id)
  {
    $sql = "SELECT * FROM carts WHERE user_id = :user_id AND item_id = :item_id AND cart_id = :cart_id AND buy_flg = 0";
    $stmt =  $this->pdo->prepare($sql);
    $stmt->bindValue(":user_id", $user_id);
    $stmt->bindValue(":item_id", $item_id);
    $stmt->bindValue(":cart_id", $cart_id);
    $res = $stmt->execute();
    if ($res) {
      return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    return false;
  }

  public function findBoughtItem($user_id, $limit = 4)
  {
    $sql = "SELECT carts.id as original_id, carts.item_id, carts.item_quantity, items.item_image, items.item_name, items.item_price, items.anime_title";
    $sql .= " FROM carts INNER JOIN items ON carts.item_id = items.item_id";
    $sql .= " WHERE user_id = :user_id AND buy_flg = 1";
    $sql .= " ORDER BY original_id DESC";
    $sql .= " LIMIT {$limit}";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':user_id', $user_id);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
  }

  public function findNowBoughtItem($user_id, $cart_id)
  {
    $sql = "SELECT carts.item_id, carts.item_quantity, items.item_image, items.item_name, items.item_price, items.anime_title";
    $sql .= " FROM carts INNER JOIN items ON carts.item_id = items.item_id";
    $sql .= " WHERE user_id = :user_id AND cart_id = :cart_id AND buy_flg = 1";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':user_id', $user_id);
    $stmt->bindValue(':cart_id', $cart_id);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
  }

  public function insert($user_id, $item_id, $item_quantity, $cart_id)
  {
    try {
      $this->pdo->beginTransaction();

      $item_con = new Item();
      if (!$item_con->getItemById($item_id)) {
        throw new PDOException("不正な商品IDです。");
      }


      $sql = "INSERT INTO carts (user_id, item_id, item_quantity, cart_id)";
      $sql .= " VALUES (:user_id, :item_id, :item_quantity, :cart_id)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(":user_id", $user_id, PDO::PARAM_INT);
      $stmt->bindValue(":item_id", $item_id, PDO::PARAM_INT);
      $stmt->bindValue(":item_quantity", $item_quantity, PDO::PARAM_INT);
      $stmt->bindValue(":cart_id", $cart_id);
      $res = $stmt->execute();
      if ($res) {
        $this->pdo->commit();
      }
    } catch (PDOException $e) {
      echo $e->getMessage();
      $this->pdo->rollBack();
    }
  }

  public function update($user_id, $cart_id, $item_id, $quantity, $existItemQuantity = 0)
  {
    if ($existItemQuantity > 0) {
      $quantity = (int)$quantity + (int)$existItemQuantity;
    }

    try {
      $this->pdo->beginTransaction();

      $item_con = new Item();
      if (!$item_con->getItemById($item_id)) {
        throw new PDOException("不正な商品IDです。");
      }
      if (!$this->findByCartId($cart_id)) {
        throw new PDOException("不正なカート情報です。");
      }

      $sql = "UPDATE carts";
      $sql .= " SET item_quantity = :item_quantity";
      $sql .= " WHERE user_id = :user_id AND item_id = :item_id AND cart_id = :cart_id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':item_quantity', $quantity);
      $stmt->bindValue(':user_id', $user_id);
      $stmt->bindValue(':item_id', $item_id);
      $stmt->bindValue(':cart_id', $cart_id);
      $res = $stmt->execute();
      if ($res) {
        $this->pdo->commit();
      }
    } catch (PDOException $e) {
      echo $e->getMessage();
      $this->pdo->rollBack();
    }
  }


  public function delete($user_id, $cart_id, $item_id)
  {
    try {
      $this->pdo->beginTransaction();

      $item_con = new Item();
      if (!$item_con->getItemById($item_id)) {
        throw new PDOException("不正な商品IDです。");
      }

      if (!$this->findByCartId($cart_id)) {
        throw new PDOException("不正なカート情報です。");
      }

      $sql = 'DELETE FROM carts WHERE user_id = :user_id AND cart_id = :cart_id AND item_id = :item_id AND buy_flg = 0';
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':user_id', $user_id);
      $stmt->bindValue(':cart_id', $cart_id);
      $stmt->bindValue(':item_id', $item_id);
      $res = $stmt->execute();

      if ($res) {
        $this->pdo->commit();
      }
    } catch (PDOException $e) {
      echo $e->getMessage();
      $this->pdo->rollBack();
    }
  }

  public function buy($user_id, $cart_id)
  {
    // if (!$this->findByCartId($cart_id)) {
    //   throw new PDOException("不正なカート情報です。");
    // }

    try {
      $this->pdo->beginTransaction();
      $sql = "UPDATE carts SET buy_flg = 1 WHERE user_id = :user_id AND cart_id = :cart_id";

      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':user_id', $user_id);
      $stmt->bindValue(':cart_id', $cart_id);
      $res = $stmt->execute();

      if ($res) {
        $this->pdo->commit();
      }
    } catch (PDOException $e) {
      echo $e->getMessage();
      $this->pdo->rollBack();
    }
  }
}
