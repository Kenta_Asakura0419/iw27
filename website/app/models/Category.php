<?php
require_once "Model.php";

/**
 * class Category
 */

class Category extends Model
{
  public function getCategory($category_id)
  {
    $sql = "SELECT * FROM categories WHERE category_id = :category_id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(":category_id", $category_id);
    $res = $stmt->execute();
    if ($res) {
      return $stmt->fetch(PDO::FETCH_OBJ);
    }

    return false;
  }
}
