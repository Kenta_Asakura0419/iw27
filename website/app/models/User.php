<?php
require_once 'Model.php';

// --- table_sample --- //
// id
// name
// email
// password
// address
// card
// created_at
// updated_at
// ------------------ //

class User extends Model
{
  protected $regxpEmail = '/^[a-zA-Z0-9_+-]+(.[a-zA-Z0-9_+-]+)*@([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]*\.)+[a-zA-Z]{2,}$/';
  protected $regxpPassword = '/^[a-zA-Z0-9!@#$%^&*_\-+=]{8,20}$/';

  function auth($email, $password)
  {
    $findUser = $this->findByEmail($email);
    //password_verify() で認証処理
    if ($findUser) {
      if (password_verify($password, $findUser['password'])) {
        // ユーザー情報をセッションに保存
        Session::save('authUser', $findUser);

        //成功したら true を返す
        return true;
      };
    }
  }

  function logout()
  {
    Session::clear('authUser');
    URL::redirect(PAGES);
  }

  function validateRegist($data)
  {
    $errors = [];
    if (empty($data['name'])) {
      $errors['name'] = 'ユーザー名を入力してください';
    }

    if (empty($data['email'])) {
      $errors['email'] = 'Eメールを入力してください';
    } elseif (!preg_match($this->regxpEmail, $data['email'])) {
      $errors['email'] = "無効なメールアドレスです。";
    }

    if (empty($data['password'])) {
      $errors['password'] = 'パスワードを入力してください';
    } elseif (!preg_match($this->regxpPassword, $data['password'])) {
      $errors['password'] = "無効なパスワードです。<br/>8~20文字で半角英数字と!@#$%^&*_-+=が使えます。";
    }

    return $errors;
  }

  function validateEdit($data)
  {
    $errors = [];
    if (empty($data['name'])) {
      $errors['name'] = 'ユーザー名を入力してください';
    }

    if (empty($data['email'])) {
      $errors['email'] = 'Eメールを入力してください';
    } elseif (!preg_match($this->regxpEmail, $data['email'])) {
      $errors['email'] = "無効なメールアドレスです。";
    }

    if (empty($data['new_password'])) {
      $errors['new_password'] = '新しいパスワードを入力してください';
    } elseif (!preg_match($this->regxpPassword, $data['new_password'])) {
      $errors['new_password'] = "無効なパスワードです。<br/>8~20文字で半角英数字と!@#$%^&*_-+=が使えます。";
    }

    if ($data['new_password'] !== $data['new_password_confirm']) {
      $errors['new_password_confirm'] = 'パスワードが一致していません';
    }

    return $errors;
  }

  function findByEmail($email)
  {
    $sql = "SELECT * FROM users WHERE email = :email";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':email', $email);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    return $user;
  }

  function insert($data)
  {
    $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
    $sql = 'INSERT INTO users (name, email, password) VALUES (:name, :email, :password)';
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':name', $data['name']);
    $stmt->bindValue(':email', $data['email']);
    $stmt->bindValue(':password', $data['password']);
    return $stmt->execute();
  }

  function updateUser($data)
  {
    $data['new_password'] = password_hash($data['new_password'], PASSWORD_BCRYPT);
    date_default_timezone_set('Asia/Tokyo');
    $updated_at = date("Y/m/d H:i:s");
    $sql =
      'UPDATE users' .
      ' SET name = :name, email = :email, password = :password, updated_at = :updated_at' .
      ' WHERE id = :id';
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $data['id']);
    $stmt->bindValue(':name', $data['name']);
    $stmt->bindValue(':email', $data['email']);
    $stmt->bindValue(':password', $data['new_password']);
    $stmt->bindValue(':updated_at', $updated_at);
    return $stmt->execute();
  }

  function deleteUser($data)
  {
    $sql =
      'DELETE FROM users WHERE id = :id';
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $data['id']);
    return $stmt->execute();
  }

  function allUser($limit = 20, $offset = 0)
  {
    $users = [];
    $sql = "SELECT * FROM users ORDER BY id DESC LIMIT {$limit} OFFSET {$offset};";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    $users = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $users;
  }


  function count()
  {
    $sql = "SELECT count(id) AS count FROM users;";
    $row = $this->pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
    return $row['count'];
  }
}
