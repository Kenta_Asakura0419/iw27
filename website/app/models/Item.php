<?php
require_once 'Model.php';

// --- table_sample --- //
// item_id
// item_name
// item_image
// anime_title
// category_id
// item_price
// item_stock
// item_description
// created_at
// updated_at
// ------------------ //

class Item extends Model
{
  function allItems($limit = 20, $offset = 0, $sort = "DESC")
  {
    $items = [];
    $sql =
      'SELECT item_id, item_name, item_image, anime_title, category_name, item_price, item_stock, item_description, created_at, updated_at' .
      ' FROM items' .
      ' INNER JOIN categories' .
      ' ON items.category_id = categories.category_id' .
      " ORDER BY item_id {$sort}" .
      " LIMIT {$limit} OFFSET {$offset};";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    $items = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $items;
  }

  function getItemsBySearch($search_query, $limit = null, $offset = null)
  {
    // 全角処理と空白のトリミング
    $search_query = preg_replace('/\A[\x00\s]++|[\x00\s]++\z/u', '', $search_query);
    $search_query = mb_convert_kana($search_query, 'KVa');

    // 2語以上の検索を禁止
    $split = str_replace([" ", "　", ",", "、"], ',', $search_query);
    $keywords = explode(',', $split);
    if (count($keywords) > 1) {
      return [];
    }


    $keyword = "%$search_query%";
    $sql = "SELECT * from items";
    $sql .= " WHERE item_name LIKE ?";
    $sql .= " OR anime_title LIKE ?";
    $sql .= " OR item_id LIKE ?";
    if (!is_null($limit) && !is_null($limit)) {
      $sql .= " LIMIT {$limit} OFFSET {$offset}";
    }

    $stmt = $this->pdo->prepare($sql);
    for ($i = 0; $i < mb_substr_count($sql, '?'); $i++) {
      $stmt->bindValue($i + 1, $keyword);
    }

    $res = $stmt->execute();

    if ($res) {
      return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    return false;
  }

  function getItemById($id)
  {
    $sql = "SELECT * from items WHERE item_id = :id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $res = $stmt->execute();

    if ($res) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    return $data;
  }

  function getItemByCategory($category_id, $limit = 200, $offset = 0)
  {
    $sql = "SELECT * from items WHERE category_id = :category_id LIMIT :limit OFFSET :offset";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindParam(':category_id', $category_id, PDO::PARAM_INT);
    $stmt->bindParam(':limit', $limit);
    $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
    $res = $stmt->execute();

    if ($res) {
      $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    return $data;
  }

  function getItemsByAuthor($author_id)
  {
    $sql = "SELECT items.item_id, items.item_name, items.item_image, items.anime_title, items.item_price" .
      " FROM ( items INNER JOIN animes ON items.anime_title = animes.anime_title )" .
      " WHERE animes.anime_author = :author_id";
    // " INNER JOIN authors ON animes.anime_author = authors.author_id" .
    // " WHERE authors.author_id = :author_id";
    $stmt = $this->pdo->prepare($sql);

    $stmt->bindParam(':author_id', $author_id);
    // $stmt->bindParam(':limit', $limit);
    // $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
    $res = $stmt->execute();

    if ($res) {
      $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    return $data;
  }

  function sortItems($anime_title, $category_id)
  {
    $sql = "SELECT * from items WHERE anime_title = :anime_title AND category_id = :category_id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindParam(':anime_title', $anime_title);
    $stmt->bindParam(':category_id', $category_id, PDO::PARAM_INT);
    $res = $stmt->execute();

    if ($res) {
      $data = $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    return $data;
  }

  function categories()
  {
    $categories = [];
    $sql = "SELECT * FROM categories";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    $categories = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $categories;
  }

  function divideImages(string $images)
  {
    $img_array = explode(",", $images);

    $keyvisual = array_shift($img_array);

    $dividedImages = [$keyvisual, $img_array];

    return $dividedImages;
  }

  function insert($data, $image)
  {
    $imageName = implode(',', $image['name']);

    $sql =
      'INSERT INTO items (item_name, item_image, anime_title, category_id, item_price, item_stock, item_description)' .
      ' VALUES (:item_name, :item_image, :anime_title, :category_id, :item_price, :item_stock, :item_description)';
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':item_name', $data['item_name']);
    $stmt->bindValue(':item_image', $imageName);
    $stmt->bindValue(':anime_title', $data['anime_title']);
    $stmt->bindValue(':category_id', $data['category_id'], PDO::PARAM_INT);
    $stmt->bindValue(':item_price', $data['item_price'], PDO::PARAM_INT);
    $stmt->bindValue(':item_stock', $data['item_stock'], PDO::PARAM_INT);
    $stmt->bindValue(':item_description', $data['item_description']);
    return $stmt->execute();
  }

  function moveUploadedFile($files, $basePath)
  {

    $counts = count($files['name'] ?? []);

    for ($i = 0; $i < $counts; $i++) {
      $tmpFile = $files['tmp_name'][$i] ?? '';
      $orgFile = $files['name'][$i] ?? '';
      if ($tmpFile != "" && $orgFile != ""  && is_uploaded_file($tmpFile)) {
        move_uploaded_file($tmpFile, $basePath . $orgFile);
      }
    }
  }

  function update($data, $image)
  {
    $imageName = implode(',', $image['name']);
    date_default_timezone_set('Asia/Tokyo');
    $updated_at = date("Y/m/d H:i:s");
    $sql =
      'UPDATE items' .
      ' SET item_name = :item_name, item_image = :item_image, anime_title = :anime_title, category_id = :category_id, item_price = :item_price, item_stock = :item_stock, item_description = :item_description, updated_at = :updated_at' .
      ' WHERE item_id = :item_id';
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':item_id', $data['item_id']);
    $stmt->bindValue(':item_name', $data['item_name']);
    $stmt->bindValue(':item_image', $imageName);
    $stmt->bindValue(':anime_title', $data['anime_title']);
    $stmt->bindValue(':category_id', $data['category_id'], PDO::PARAM_INT);
    $stmt->bindValue(':item_price', $data['item_price'], PDO::PARAM_INT);
    $stmt->bindValue(':item_stock', $data['item_stock'], PDO::PARAM_INT);
    $stmt->bindValue(':item_description', $data['item_description']);
    $stmt->bindValue(':updated_at', $updated_at);
    return $stmt->execute();
  }

  function delete($item_id)
  {
    $sql =
      'DELETE FROM items WHERE item_id = :item_id';
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':item_id', $item_id);
    return $stmt->execute();
  }

  function validate($data, $files)
  {
    $errors = [];
    if (empty($files['name'][0])) {
      $errors['item_image'] = '画像を選択してください';
    }

    if (empty($data['item_name'])) {
      $errors['item_name'] = '商品名を入力してください';
    }
    if (empty($data['anime_title'])) {
      $errors['anime_title'] = '作品名を入力してください';
    }
    if (empty($data['item_price'])) {
      $errors['item_price'] = '価格を入力してください';
    }
    if (empty($data['item_stock'])) {
      $errors['item_stock'] = '在庫数を入力してください';
    }
    if (empty($data['item_description'])) {
      $errors['item_description'] = '説明文を入力してください';
    }


    return $errors;
  }

  function count($category_id = null)
  {
    if (is_null($category_id)) {
      $sql = "SELECT count(item_id) AS count FROM items;";
    } else {
      $sql = "SELECT count(item_id) AS count FROM items WHERE category_id = {$category_id};";
    }
    $row = $this->pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
    return $row['count'];
  }
}
