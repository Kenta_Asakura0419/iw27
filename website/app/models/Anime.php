<?php
require_once "Model.php";

/**
 * class Anime
 */

// --- table_sample --- //
// anime_id
// anime_title
// anime_author
// anime_description
// anime_startday
// anime_hp
// ------------------ //

class Anime extends Model
{
  public function fetchAllAnimes($limit = null)
  {
    $sql = '';
    if (is_null($limit)) {
      $sql =
        "SELECT * FROM animes " .
        "INNER JOIN authors " .
        "ON animes.anime_author = authors.author_id";
    } else {
      $sql =
        "SELECT * FROM animes " .
        "INNER JOIN authors " .
        "ON animes.anime_author = authors.author_id" .
        " LIMIT {$limit}";
    }

    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    $animes = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $animes;
  }

  public function fetchOneAnime($anime_id)
  {
    $sql =
      "SELECT * FROM animes " .
      "INNER JOIN authors " .
      "ON animes.anime_author = authors.author_id " .
      "AND animes.anime_id = :anime_id";

    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue('anime_id', $anime_id);
    $stmt->execute();
    $anime = $stmt->fetch(PDO::FETCH_OBJ);
    return $anime;
  }

  public function fetchAllAuthors($limit = null)
  {
    if (is_null($limit)) {
      $sql =
        "SELECT * FROM authors";
    } else {
      $sql =
        "SELECT * FROM authors" .
        " LIMIT {$limit}";
    }

    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    $authors = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $authors;
  }

  public function fetchOneAuthor($author_id)
  {
    $sql =
      "SELECT * FROM authors " .
      "WHERE author_id = :author_id";

    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue('author_id', $author_id);
    $stmt->execute();
    $author = $stmt->fetch(PDO::FETCH_OBJ);
    return $author;
  }
}
