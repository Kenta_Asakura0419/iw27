<div class="c-404">
  <h1 class="c-404__title">404 Not Found</h1>
  <p class="c-404__subtitle">お探しのページは見つかりませんでした。</p>
</div>