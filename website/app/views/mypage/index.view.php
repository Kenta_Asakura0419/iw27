<div class="c-mypage">
  <div class="c-mypage__logout">
    <button><a href=<?= URL::route('logout/') ?>>ログアウト</a></button>
    <button><a href=<?= URL::route('mypage/user_delete') ?>>退会する</a></button>
  </div>
  <h2 class="c-mypage__title">マイページ</h2>

  <!-- <div class="c-mypage__section">
    <h3 class="c-mypage__sectionTitle">お気に入り</h3>
    <ul class="c-mypage__listContainer">
      <?php foreach ($items as $currentItem) : ?>
        <li class="c-mypage__list">
          <div class="c-item">
            <a href=<?= URL::route("goods/?goods_id={$currentItem->item_id}") ?>>
              <div class="c-item__image">
                <?php $keyvisual = $item->divideImages($currentItem->item_image)[0] ?>
                <img src=<?= ASSETS . "images/" . $keyvisual ?> alt="商品画像">
              </div>
              <h4>
                <p class="c-item__worksName"><?= $currentItem->anime_title ?></p>
                <p class="c-item__name"><?= $currentItem->item_name ?></p>
              </h4>
              <p class="c-item__plice">&yen;<?= $currentItem->item_price ?></p>
            </a>
          </div>
        </li>
      <?php endforeach ?>
    </ul>
  </div> -->

  <div class="c-mypage__section">
    <h3 class="c-mypage__sectionTitle">チェックした商品</h3>
    <?php if (isset($exploredItems)) : ?>
      <ul class="c-mypage__listContainer">
        <?php foreach ($exploredItems as $currentItem) : ?>
          <li class="c-mypage__list">
            <div class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$currentItem["item_id"]}") ?>>
                <div class="c-item__image">
                  <?php $keyvisual = $item->divideImages($currentItem["item_image"])[0] ?>
                  <img src=<?= ASSETS . "images/" . $keyvisual ?> alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $currentItem["anime_title"] ?></p>
                  <p class="c-item__name"><?= $currentItem["item_name"] ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $currentItem["item_price"] ?></p>
              </a>
            </div>
          </li>
        <?php endforeach ?>
      </ul>
    <?php else : ?>
      <p style="text-align: center; margin-top: 20px;">最近閲覧した商品はありません。</p>
    <?php endif; ?>
  </div>

  <section class="c-mypage__section">
    <h3 class="c-mypage__sectionTitle">購入履歴</h3>
    <?php if ($currentBuy !== []) : ?>
      <ul class="c-mypage__listContainer">
        <?php foreach ($currentBuy as $currentItem) : ?>
          <li class="c-mypage__list">
            <div class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$currentItem->item_id}") ?>>
                <div class="c-item__image">
                  <?php $keyvisual = $item->divideImages($currentItem->item_image)[0] ?>
                  <img src=<?= ASSETS . "images/" . $keyvisual ?> alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $currentItem->anime_title ?></p>
                  <p class="c-item__name"><?= $currentItem->item_name ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $currentItem->item_price ?></p>
              </a>
            </div>
          </li>
        <?php endforeach ?>
      </ul>
    <?php else : ?>
      <p style="text-align: center; margin-top: 20px;">最近購入した商品はありません。</p>
    <?php endif; ?>
  </section>

  <section class="c-mypage__userInfoEditContainer">
    <h2 class="c-mypage__title">お客様情報編集</h2>
    <ul class="c-mypage__editList">
      <li>
        <a href="<?= URL::route("mypage/user_edit") ?>">
          <button class="c-mypage__editButton">会員情報編集</button>
        </a>
      </li>
      <!-- <li>
        <button class="c-mypage__editButton">
          <a href="<?= URL::route("mypage/address_edit") ?>">お届け先情報編集</a>
        </button>
      </li>
      <li>
        <button class="c-mypage__editButton">
          <a href="<?= URL::route("mypage/card_edit") ?>">クレジットカード編集</a>
        </button>
      </li> -->
    </ul>
  </section>
</div>