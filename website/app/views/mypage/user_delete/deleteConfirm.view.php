<div class="c-form__wrapper p-result">
  <h2 class="c-form__headding">退会確認</h2>
  <div class="c-form__error">退会いたしますか？</div>
  <a href="<?= URL::route('mypage/user_delete/delete.php') ?>" class="c-btn c-btn--primary">退会する</a>
  <a href="<?= URL::route('mypage') ?>" class="c-btn c-btn--primary">戻る</a>
</div>