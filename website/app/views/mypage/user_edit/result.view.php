<div class="c-form__wrapper p-result">
  <h2 class="c-form__headding">会員情報変更完了</h2>
  <p class="c-form__error">セキュリティのため自動的にログアウトしました。</p>
  <a href="<?= URL::route('login/') ?>" class="c-btn c-btn--primary">ログイン画面へ</a>
</div>