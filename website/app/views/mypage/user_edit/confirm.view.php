<div class="c-form__wrapper">
  <h2 class="c-form__headding">会員情報変更確認画面</h2>
  <form action="result.php" method="POST">
    <div class="c-form__content p-regist__confirm">
      <p>ユーザー名</p>
      <p><?= @$posts['name'] ?></p>
    </div>
    <div class="c-form__content p-regist__confirm">
      <p>メールアドレス</p>
      <p><?= @$posts['email'] ?></p>
    </div>
    <div class="c-form__content p-regist__confirm">
      <p>パスワード</p>
      <p> セキュリティ保護のため表示できません</p>
    </div>
    <button class="c-btn c-btn--primary">変更する</button>
    <a href="index.php" class="c-btn c-btn--primary">戻る</a>
  </form>
</div>