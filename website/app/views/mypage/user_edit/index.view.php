<div class="c-form__wrapper">
  <h2 class="c-form__headding">会員情報変更</h2>
  <?php if (isset($editError)) : ?>
    <p class="c-form__error"><?= $editError ?></p>
  <?php endif ?>
  <form action="confirm.php" method="POST">
    <div class="c-form__content">
      <label class="c-form__label" for="name">ユーザー名 <span class="c-form__required">※必須</span></label>
      <input type="text" id="name" class="c-form__input" name="name" value="<?= @$user->value['name'] ?>">
      <p class="c-form__error"><?= @$errors['name'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="email">メールアドレス <span class="c-form__required">※必須</span></label>
      <input type="email" id="email" class="c-form__input" name="email" value="<?= @$user->value['email'] ?>">
      <p class="c-form__error"><?= @$errors['email'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="new_password">新しいパスワード <span class="c-form__required">※必須</span></label>
      <input type="password" id="new_password" class="c-form__input js-eye__input" name="new_password">
      <ion-icon class="c-form__eye js-eye" name="eye-off-outline"></ion-icon>
      <p class="c-form__error"><?= @$errors['new_password'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="new_password_confirm">新しいパスワード(確認) <span class="c-form__required">※必須</span></label>
      <input type="password" id="new_password_confirm" class="c-form__input js-eye__input" name="new_password_confirm">
      <ion-icon class="c-form__eye js-eye" name="eye-off-outline"></ion-icon>
      <p class="c-form__error"><?= @$errors['new_password_confirm'] ?></p>
    </div>

    <input type="hidden" name="id" value="<?= @$user->value['id'] ?>">

    <button class="c-btn c-btn--primary u-w100">確認画面へ</button>
  </form>
</div>