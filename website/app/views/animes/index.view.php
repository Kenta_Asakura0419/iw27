<section class="c-category">
  <h3 class="c-category_title">作品一覧</h3>
  <ul class="c-category_container">
    <?php foreach ($animes as $anime) : ?>
      <li class="c-category_worksList">
        <a href="<?= URL::route("animes/?anime_id={$anime->anime_id}") ?>">
          <button class="c-category_worksButton">
            <?= $anime->anime_title ?>
          </button>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>
</section>