<?php

/** @var Item $item */
?>
<?php if ($data->anime_title !== "その他複数") : ?>
  <div class="c-worksIntroduction">
    <section class="c-worksIntroduction__container">
      <div class="c-worksIntroduction__about">
        <img src=<?= ASSETS . "images/animes/{$data->anime_image}" ?> alt=<?= $data->anime_title ?>>
        <div>
          <h2 class="c-worksIntroduction__title">
            <?= $data->anime_title ?>
          </h2>
          <div class="c-worksIntroduction__detail">
            <p>原作： <?= $data->author_name ?></p>
            <p>連載開始日： <?= str_replace('-', '/', $data->anime_startday)  ?></p>
          </div>
        </div>
      </div>

      <article class="c-worksIntroduction__summaryContainer">
        <p class="c-worksIntroduction__summaryTitle">あらすじ</p>
        <p class="c-worksIntroduction__summary">
          <?= $data->anime_description ?>
        </p>
      </article>
    </section>

    <?php if ($data->anime_hp !== "" or $data->author_twitter !== "") : ?>
      <ul class="c-snsList">
        <?php if ($data->anime_hp !== "") : ?>
          <li>
            <a href=<?= $data->anime_hp ?> target="_blank" rel="noopener noreferrer">
              <button class="c-snsList_homepage">作品ホームページ</button>
            </a>
          </li>
        <?php endif; ?>
        <?php if ($data->author_twitter !== "") : ?>
          <li>
            <a href=<?= $data->author_twitter ?> target="_blank" rel="noopener noreferrer">
              <button class="c-snsList_twitter">
                <i class="fab fa-twitter"></i>
                作者SNS
              </button>
            </a>
          </li>
        <?php endif; ?>
      </ul>
    <?php endif ?>
  </div>
<?php else : ?>
  <div style="padding-top: 80px;">
    <h2 class="c-worksIntroduction__title" style="text-align: center;">
      <?= $data->anime_title ?>
    </h2>
  </div>
<?php endif ?>

<div class="c-sortableGoodsList">
  <h3 class="c-sortableGoodsList__title">
    グッズ一覧
  </h3>

  <?php foreach ($categories as $key => $category) : ?>
    <input id=<?= 'cat' . $category->category_id ?> type="radio" name="tab_btn" <?= $key === 0 ? 'checked' : '' ?>>
  <?php endforeach; ?>

  <ul class="c-sortableGoodsList__sortList">
    <?php foreach ($categories as $category) : ?>

      <label for=<?= 'cat' . $category->category_id ?> id=<?= 'cat' . $category->category_id . '_label' ?> class="c-sortableGoodsList__button">
        <?= $category->category_name ?>
      </label>
    <?php endforeach; ?>
  </ul>

  <div class="c-itemList c-itemList__listContainer">
    <?php foreach ($categories as $category) : ?>

      <div id=<?= 'panel' . $category->category_id ?> class="c-itemList__panel">
        <?php $values = $item->sortItems($data->anime_title, $category->category_id); ?>

        <?php if (empty($values)) : ?>
          <p>登録された商品が見つかりませんでした。</p>
        <?php endif; ?>

        <?php foreach ($values as $value) : ?>
          <section class="c-item">
            <a href=<?= URL::route("goods/?goods_id=" . $value->item_id) ?>>
              <div class="c-item__image">
                <?php $keyvisual = $item->divideImages($value->item_image)[0] ?>
                <img src=<?= ASSETS . "images/" .  $keyvisual ?> alt="商品画像">
              </div>
              <h4>
                <p class="c-item__worksName"><?= $value->anime_title ?></p>
                <p class="c-item__name"><?= $value->item_name ?></p>
              </h4>
              <p class="c-item__plice">￥<?= $value->item_price ?></p>
            </a>
          </section>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
  <!-- <div class="c-btn__viewmore"> -->
  <!-- css確認 -->
  <!-- <button>もっと見る</button> -->
  <!-- </div> -->
</div>

<div class="c-itemListWrapper__anime">
  <div class="c-byWorkBookList c-itemList">
    <h3 class="c-itemList__title">
      単行本・公式ファンブック
    </h3>
    <ul class="c-itemList__listContainer">
      <?php foreach ($books as $book) : ?>
        <li class="c-itemList__list">
          <section class="c-item">
            <a href=<?= URL::route("goods/?goods_id=" . $book->item_id) ?>>
              <div class="c-item__image c-item__commic">
                <?php $keyvisual = $item->divideImages($book->item_image)[0] ?>
                <img src=<?= ASSETS . "images/" . $keyvisual ?> alt="商品画像">
              </div>
              <h4>
                <p class="c-item__worksName"><?= $book->anime_title ?></p>
                <p class="c-item__name"><?= $book->item_name ?></p>
              </h4>
              <p class="c-item__plice">&yen;<?= $book->item_price ?></p>
            </a>
          </section>
        </li>
      <?php endforeach ?>
    </ul>
    <!-- <div class="c-btn__viewmore">
      <button>もっと見る</button>
    </div> -->
  </div>
</div>