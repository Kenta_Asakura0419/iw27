<?php
require_once "app/models/Anime.php";
$animeModel = new Anime();
$animes = $animeModel->fetchAllAnimes();
$authors = $animeModel->fetchAllAuthors();
?>

<footer class="l-footer">
  <div class="l-footer__pc">
    <img src=<?= ASSETS . "images/logo.svg"  ?> alt="ロゴ" class="c-footer__logo">


    <ul class="c-nav__linkIcon">
      <li>
        <a href="<?= PAGES ?>">
          <button class="c-nav__icon c-nav__topIcon">
            <p>トップ</p>
          </button>
        </a>
      </li>
      <?php if (isset($_SESSION['authUser'])) : ?>
        <li>
          <a href="<?= URL::route("mypage/") ?>">
            <button class="c-nav__icon c-nav__mypageIcon">
              <p>マイページ</p>
            </button>
          </a>
        </li>

        <?php if ($_SESSION['authUser']['name'] === 'admin') : ?>
          <li>
            <a href="<?= URL::route('admin/') ?>">
              <button class="c-nav__icon c-nav__adminIcon">
                <p>管理画面</p>
              </button>
            </a>
          </li>
        <?php else : ?>
          <li>
            <a href=<?= URL::route("cart/") ?>>
              <button class="c-nav__icon c-nav__cartIcon">
                <p>カート</p>
              </button>
            </a>
          </li>
        <?php endif  ?>

      <?php else : ?>
        <li>
          <a href="<?= URL::route("login/") ?>">
            <button class="c-nav__icon c-nav__loginIcon">
              <p>ログイン</p>
            </button>
          </a>
        </li>
        <li>
          <a href="<?= URL::route("regist/") ?>">
            <button class="c-nav__icon c-nav__registIcon">
              <p>会員登録</p>
            </button>
          </a>
        </li>
      <?php endif; ?>
    </ul>
  </div>


  <div class="l-footer__sp">
    <div class="c-footerSNS__navigationButton">
      <button>
        <p>トップ</p>
      </button>
      <button>
        <p>マイページ</p>
      </button>
      <button>
        <p>カート</p>
      </button>
    </div>
  </div>
</footer>