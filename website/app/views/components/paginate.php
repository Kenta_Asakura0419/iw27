<?php if (isset($params)) : ?>
  <nav>
    <ul class="c-pagination">
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=1<?= $params ?>">&laquo;</a></li>
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page_prev . $params ?>">Prev</a></li>
      <?php foreach ($pages as $page) : ?>
        <?php if ($current_page == $page) : ?>
          <li class="c-pagination__item active"><a class="c-pagination__link" href="?page=<?= $page . $params ?>"><?= $page ?></a></li>
        <?php else : ?>
          <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page . $params ?>"><?= $page ?></a></li>
        <?php endif ?>
      <?php endforeach ?>
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page_next . $params ?>">Next</a></li>
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page_count . $params ?>">&raquo;</a></li>
    </ul>
  </nav>
<?php else : ?>
  <nav>
    <ul class="c-pagination">
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=1">&laquo;</a></li>
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page_prev ?>">Prev</a></li>
      <?php foreach ($pages as $page) : ?>
        <?php if ($current_page == $page) : ?>
          <li class="c-pagination__item active"><a class="c-pagination__link" href="?page=<?= $page ?>"><?= $page ?></a></li>
        <?php else : ?>
          <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page ?>"><?= $page ?></a></li>
        <?php endif ?>
      <?php endforeach ?>
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page_next ?>">Next</a></li>
      <li class="c-pagination__item"><a class="c-pagination__link" href="?page=<?= $page_count ?>">&raquo;</a></li>
    </ul>
  </nav>
<?php endif; ?>