<header class="l-header js-onView">
  <div class="l-header__container">
    <div class="c-header__left">
      <h1>
        <a href="<?= PAGES ?>">
          <img src=<?= ASSETS . "images/logo.svg" ?> alt="きららストア" class="c-headLogo">
        </a>
      </h1>
      <form action=<?= htmlspecialchars(URL::route(""), ENT_QUOTES, 'UTF-8') ?> class="c-searchBar">
        <input type="text" name="search_query" class="c-searchBar__form" placeholder="検索" value=<?= isset($_GET["search_query"]) ? $_GET["search_query"] : "" ?>>
        <button class="c-searchBar__button"><img src="<?= ASSETS . "images/search_icon.svg" ?>" alt="検索"></button>
      </form>
    </div>
    <nav class="c-nav">
      <div class="c-nav__serch">
        <a href="<?= PAGES . 'animes/' ?>">
          <button class="c-nav__searchButton">作品から探す</button>
        </a>
        <a href="<?= PAGES . 'authors/' ?>">
          <button class="c-nav__searchButton">作者から探す</button>
        </a>
      </div>
      <ul class="c-nav__linkIcon">
        <li>
          <a href="<?= PAGES ?>">
            <button class="c-nav__icon c-nav__topIcon">
              <p>トップ</p>
            </button>
          </a>
        </li>
        <?php if (isset($_SESSION['authUser'])) : ?>
          <li>
            <a href="<?= URL::route("mypage/") ?>">
              <button class="c-nav__icon c-nav__mypageIcon">
                <p>マイページ</p>
              </button>
            </a>
          </li>

          <?php if ($_SESSION['authUser']['name'] === 'admin') : ?>
            <li>
              <a href="<?= URL::route('admin/') ?>">
                <button class="c-nav__icon c-nav__adminIcon">
                  <p>管理画面</p>
                </button>
              </a>
            </li>
          <?php else : ?>
            <li>
              <a href=<?= URL::route("cart/") ?>>
                <button class="c-nav__icon c-nav__cartIcon">
                  <p>カート</p>
                </button>
              </a>
            </li>
          <?php endif  ?>

        <?php else : ?>
          <li>
            <a href="<?= URL::route("login/") ?>">
              <button class="c-nav__icon c-nav__loginIcon">
                <p>ログイン</p>
              </button>
            </a>
          </li>
          <li>
            <a href="<?= URL::route("regist/") ?>">
              <button class="c-nav__icon c-nav__registIcon">
                <p>会員登録</p>
              </button>
            </a>
          </li>
        <?php endif; ?>
      </ul>
    </nav>

    <div class="c-humberger js-humbergerButton">
      <span></span>
      <span></span>
      <span></span>
    </div>

    <?php include_once 'app/views/components/humberger.php' ?>
  </div>
</header>