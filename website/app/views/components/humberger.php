<div class="c-humberger__wrapper js-humberger">
  <div class="c-humberger__container">
    <ul class="c-nav__linkIcon_humberger">
      <li>
        <a href="<?= PAGES ?>">
          <button class="c-nav__icon-w c-nav__topIcon-w">
            <p>トップ</p>
          </button>
        </a>
      </li>
      <?php if (isset($_SESSION['authUser'])) : ?>
        <li>
          <a href="<?= URL::route("mypage/") ?>">
            <button class="c-nav__icon-w c-nav__mypageIcon-w">
              <p>マイページ</p>
            </button>
          </a>
        </li>

        <?php if ($_SESSION['authUser']['name'] === 'admin') : ?>
          <li>
            <a href="<?= URL::route('admin/') ?>">
              <button class="c-nav__icon-w c-nav__adminIcon-w">
                <p>管理画面</p>
              </button>
            </a>
          </li>
        <?php else : ?>
          <li>
            <a href=<?= URL::route("cart/") ?>>
              <button class="c-nav__icon-w c-nav__cartIcon-w">
                <p>カート</p>
              </button>
            </a>
          </li>
        <?php endif  ?>

      <?php else : ?>
        <li>
          <a href="<?= URL::route("login/") ?>">
            <button class="c-nav__icon-w c-nav__loginIcon-w">
              <p>ログイン</p>
            </button>
          </a>
        </li>
        <li>
          <a href="<?= URL::route("regist/") ?>">
            <button class="c-nav__icon-w c-nav__registIcon-w">
              <p>会員登録</p>
            </button>
          </a>
        </li>
      <?php endif; ?>
    </ul>

    <div class="c-humberger__searchBy">
      <div>
        <a href="<?= PAGES . 'animes/' ?>">
          <button class="c-humberger__searchButton">作品から探す</button>
        </a>
      </div>

      <div>
        <a href="<?= PAGES . 'authors/' ?>">
          <button class="c-humberger__searchButton">作者から探す</button>
        </a>
      </div>
    </div>
  </div>
</div>