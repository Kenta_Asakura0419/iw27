<!DOCTYPE html>
<html lang="ja">

<?php include 'app/views/components/head.php' ?>

<body>
  <?php include 'app/views/components/header.php' ?>

  <main class="l-main">

    <?php include $template ?>
  </main>

  <?php include 'app/views/components/footer.php' ?>
  <script src="../assets/js/swiper.js"></script>
</body>

</html>