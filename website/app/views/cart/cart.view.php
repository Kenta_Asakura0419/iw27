<div class="inCartContents">
  <h2 class="c-sectionTitle">カート情報</h2>
  <?php if ($stockItems !== []) : ?>
    <ul class="c-inCartContents__list">
      <?php foreach ($stockItems as $currentItem) : ?>
        <li>
          <div class="c-inCartContents__contents">
            <?php $keyVisual = $item->divideImages($currentItem->item_image)[0] ?>
            <img src=<?= ASSETS . "images/" . $keyVisual ?> alt="商品画像">
            <div class="c-inCartContents__contentInfo">
              <p class="c-inCartContents__title"><?= $currentItem->item_name ?></p>
              <p class="c-inCartContents__price">単価：¥<span class="js-price"><?= $currentItem->item_price ?></span>(税抜)</p>
              <div class="c-inCartContents__editQuantity">
                <p>数量:</p>
                <input type="number" name="update_quantity" id="" value=<?= $currentItem->item_quantity ?> class="c-inCartContents__quantity">
                <div>
                  <a class="c-btn__update" href=<?= URL::route("cart/?item_id={$currentItem->item_id}&update=true") ?>><button>更新</button></a>
                  <!-- <button class="c-btn__plusminus">＋</button>
                <button class="c-btn__plusminus">ー</button> -->
                </div>
              </div>
              <p class="c-inCartContents__subtotal">
                小計：&yen;<span class="js-smTotal"></span>(税抜)
              </p>
              <a href=<?= URL::route("cart/?item_id={$currentItem->item_id}&delete=true") ?>><button class="c-inCartContents__delete">削除</button></a>
            </div>
          </div>
        </li>
      <?php endforeach ?>

    </ul>
    <form action=<?= URL::route("order/") ?> style="text-align: center;">
      <input type="hidden" name="c" value=<?= $cart_id_hash ?>>
      <button class="c-inCartContents__oderButton">
        ご注文手続き
      </button>
    </form>

  <?php else : ?>
    <p style="text-align: center; margin-block: 20px;">カートの中身は空です。</p>
    <div class="c-itemList__buttons" style="margin-bottom: 50px;">
      <a href=<?= URL::route("") ?>>
        <button class="c-btn--primary">トップへ戻る</button>
      </a>
      <a href=<?= URL::route("mypage/") ?>>
        <button class="c-btn--primary">マイページへ</button>
      </a>
    </div>
  <?php endif; ?>
</div>

<script>
  const $itemPrices = document.querySelectorAll(".js-price")
  const $cartQuantities = document.querySelectorAll(".c-inCartContents__quantity")
  const $smallTotals = document.querySelectorAll(".js-smTotal")

  const itemPrices = []
  for (i = 0; i < $itemPrices.length; i++) {
    itemPrices.push($itemPrices[i].innerText)
  }

  const cartQuantities = []
  for (i = 0; i < $cartQuantities.length; i++) {
    cartQuantities.push($cartQuantities[i].value)
  }

  updateTotal()


  $cartQuantities.forEach((elm, i) => {

    // 更新URLの初期設定
    let urlWrapper = elm.nextSibling.nextSibling // input[name="update_quantity"]の次のdiv
    let baseUrlHref = urlWrapper.querySelector("a").href; // もととなるURLを保存しておく
    let urlTemplate = baseUrlHref + `&update_quantity=${elm.value}` // 更新ボタン押したときのURL
    urlWrapper.querySelector("a").href = urlTemplate // URLの更新

    elm.addEventListener('input', function(e) {
      if (elm.value < 0) {
        elm.value = 0
      }
      if (elm.value > 100) {
        elm.value = 100
      }

      cartQuantities[i] = elm.value

      urlTemplate = baseUrlHref + `&update_quantity=${elm.value}`
      urlWrapper.querySelector("a").href = urlTemplate

      updateTotal()
    })
  })

  function updateTotal() {
    for (i = 0; i < $smallTotals.length; i++) {
      (cartQuantities[i] < 0) && cartQuantities[i] == 0;
      const res = itemPrices[i] * cartQuantities[i]

      $smallTotals[i].innerText = res
    }
  };
</script>