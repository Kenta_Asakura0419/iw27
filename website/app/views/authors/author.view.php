<?php

/** @var Item $item */
?>
<div class="p-author">


  <div class="c-itemList">
    <h3 class="c-category_title"><?= $data->author_name ?>の作品のグッズ</h3>

    <?php if ($goods === []) : ?>
      <p style="text-align: center; color: red;">お探しの作家によるグッズが登録されていません。</p>
    <?php else : ?>
      <ul class="c-itemList__listContainer">
        <?php foreach ($goods as $good) : ?>
          <li class="c-itemList__list">
            <section class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$good->item_id}") ?>>
                <div class="c-item__image">
                  <?php $keyvisual = $item->divideImages($good->item_image)[0] ?>
                  <img src="<?= ASSETS . "images/{$keyvisual}" ?>" alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $good->anime_title ?></p>
                  <p class="c-item__name"><?= $good->item_name ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $good->item_price ?></p>
              </a>
            </section>
          </li>
        <?php endforeach ?>
      </ul>
      <!-- <div class="c-btn__viewmore">
      <a href=<?= URL::route("goods/?status=all") ?>><button>もっと見る</button></a>
  </div> -->
  </div>
<?php endif; ?>

</div>