<section class="c-category">
  <h3 class="c-category_title">作者一覧</h3>
  <ul class="c-category_container">
    <?php foreach ($authors as $author) : ?>
      <li class="c-category_worksList">
        <a href="<?= URL::route("authors/?author_id={$author->author_id}") ?>">
          <button class="c-category_worksButton">
            <?= $author->author_name ?>
          </button>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>
</section>