<div class="c-searchResults">
  <h2 class="c-searchResults__searchContent">
    検索結果
  </h2>

  <div class="c-category">
    <h3 class="c-category_title">カテゴリ</h3>
    <ul class="c-category_container">
      <?php foreach ($categories as $category) : ?>
        <li class="c-category_list">
          <a class="c-category_link" href="<?= URL::route("categories/?category_id={$category->category_id}") ?>">
            <button class="c-category_button">
              <?= $category->category_name ?>
            </button>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>

<div class="newItemArea c-itemList__Vgoods">
  <?php if ($data !== []) : ?>
    <ul class="c-itemList__listContainer">
      <?php foreach ($data as $currentItem) : ?>
        <li class="c-itemList__list">
          <div class="c-item">
            <a href=<?= URL::route("goods/?goods_id={$currentItem->item_id}") ?>>
              <div class="c-item__image">
                <?php $keyvisual = $item->divideImages($currentItem->item_image)[0] ?>
                <img src="<?= ASSETS . "images/{$keyvisual}" ?>" alt="商品画像">
              </div>
              <h4>
                <p class="c-item__worksName"><?= $currentItem->anime_title ?></p>
                <p class="c-item__name"><?= $currentItem->item_name ?></p>
              </h4>
              <p class="c-item__plice">&yen;<?= $currentItem->item_price ?></p>
            </a>
          </div>
        </li>
      <?php endforeach; ?>
    </ul>
    <!-- <div class="c-btn__viewmore">
      <button>もっと見る</button>
    </div> -->

    <?php include('app/views/components/paginate.php') ?>

  <?php else : ?>
    <p style="text-align: center; color: red;">お探しのグッズが登録されていないか、1語での検索をお試しください。</p>
  <?php endif; ?>


</div>