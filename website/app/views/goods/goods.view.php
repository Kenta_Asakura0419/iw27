<div class="c-goodsDetail__wrapper">
  <div class="c-goodsDetail__imageArea">
    <div class="c-goodsDetail__image">

      <img src=<?= ASSETS . 'images/' . @$keyVisual ?> alt="メインビジュアル">
    </div>
    <?php if (isset($subVisual)) : ?>
      <ul class="c-goodsDetail__imageList">
        <?php for ($i = 0; $i < count($subVisual); $i++) : ?>
          <li>
            <img src=<?= ASSETS . 'images/' . $subVisual[$i] ?> alt="画像<?= $i + 1 ?>">
          </li>
        <?php endfor; ?>
      </ul>
    <?php endif; ?>
  </div>
  <div class="c-goodsDetail__infomation">
    <h2 class="c-goodsDetail__title"><?= $data["item_name"] ?></h2>
    <div class="c-goodsDetail__feature">
      <h3>【商品詳細】</h3>
      <p>
        <?= nl2br($data["item_description"]) ?>
      </p>
    </div>
    <div class="c-goodsDetail__feature">
      <!-- <h3>【素材】</h3>
      <p>
        ・ 素材の詳細
      </p> -->
    </div>
    <div class="c-goodsDetail__smallInfo">
      <p>商品番号：　<?= $data["item_id"] ?></p>
      <!-- <p>販売期間：　〜2021年1月1日</p> -->
    </div>
    <form action=<?= URL::route("cart/") ?>>
      <div class="c-goodsDetail__priceArea">
        <p class="c-goodsDetail__price"><?= $data["item_price"] ?>円</p>
        <div class="c-goodsDetail__quantity">
          <p>数量：<input type="number" class="js-goodsQuantity" value="1" name="quantity"></p>
          <input type="hidden" name="item_id" value=<?= $data["item_id"] ?>>
          <div class="c-goodsDetail__btnArea">
            <span role="button" class="c-btn__plusminus js-quantityButton js-plusButton">＋</span>
            <span role="button" class="c-btn__plusminus js-quantityButton js-minusButton">ー</span>
          </div>
        </div>
        <p class="c-goodsDetail__subtotal">
          小計：<span><?= $data["item_price"] ?></span>円（税抜）
        </p>
      </div>
      <button class="c-btn__addToCart">カートに入れる</button>
    </form>
  </div>
</div>

<div class="c-itemList__Vgoods">
  <h3 class="c-itemList__title">関連商品</h3>
  <ul class="c-itemList__listContainer">
    <?php $refferItems = $item->sortItems($data["anime_title"], $data["category_id"]) ?>

    <?php foreach ($refferItems as $key => $value) : ?>
      <li class="c-itemList__list">
        <section class="c-item">
          <a href=<?= URL::route("goods/?goods_id={$value->item_id}") ?>>
            <div class="c-item__image">
              <?php $keyvisual = $item->divideImages($value->item_image)[0] ?>
              <img src=<?= ASSETS . "images/" . $keyvisual ?> alt="商品画像">

            </div>
            <h4>
              <p class="c-item__worksName"><?= $value->anime_title ?></p>
              <p class="c-item__name"><?= $value->item_name ?></p>
            </h4>
            <p class="c-item__plice">&yen;<?= $value->item_price ?></p>
          </a>
        </section>
      </li>
    <?php endforeach; ?>
  </ul>
  <!-- <div class="c-btn__viewmore">
    <button>もっと見る</button>
  </div> -->
</div>

<script src="../../assets/js/quantity.js"></script>