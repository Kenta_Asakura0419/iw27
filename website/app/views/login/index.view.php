<div class="c-form__wrapper">
  <h2 class="c-form__headding">ログイン</h2>
  <?php if (isset($error)) : ?>
    <p class="c-form__error"><?= $error ?></p>
  <?php endif ?>
  <form action="auth.php" method="POST">
    <div class="c-form__content">
      <label class="c-form__label" for="email">メールアドレス</label>
      <input type="email" id="email" class="c-form__input" name="email">
      <p class="c-form__error"><?= @$errors['email'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="password">パスワード</label>
      <input type="password" id="password" class="c-form__input js-eye__input" name="password">
      <ion-icon class="js-eye" name="eye-off-outline"></ion-icon>
      <p class="c-form__error"><?= @$errors['password'] ?></p>
    </div>

    <button class="c-btn c-btn--primary u-w100">ログイン</button>
  </form>
</div>