<div class="c-itemList">
  <h3 class="c-category_title"><?= $category->category_name ?></h3>

  <?php if ($items === []) : ?>
    <p style="text-align: center; color: red;">お探しのグッズが登録されていません。</p>
  <?php else :  ?>
    <ul class="c-itemList__listContainer">

      <?php if (isset($items)) : ?>
        <?php foreach ($items as $currentItem) : ?>
          <li class="c-itemList__list">
            <section class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$currentItem->item_id}") ?>>
                <div class="c-item__image">
                  <?php $keyvisual = $item->divideImages($currentItem->item_image)[0] ?>
                  <img src="<?= ASSETS . "images/{$keyvisual}" ?>" alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $currentItem->anime_title ?></p>
                  <p class="c-item__name"><?= $currentItem->item_name ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $currentItem->item_price ?></p>
              </a>
            </section>
          </li>
        <?php endforeach ?>
      <?php endif ?>
    </ul>
    <?php include('app/views/components/paginate.php') ?>

  <?php endif ?>
</div>