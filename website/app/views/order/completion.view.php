<div class="c-completion">
  <p class="c-completion__text">
    注文が確定しました。ありがとうございました。
  </p>
  <p class="c-completion__id">注文番号： <?= $cart_id ?></p>
  <ul class="c-completion__purchased">
    <?php foreach ($buy_items as  $currentItem) : ?>
      <li class="c-itemList__list">
        <div class="c-item">
          <a href=<?= URL::route("goods/goods_id={$currentItem->item_id}") ?>>
            <div class="c-item__image">
              <?php $keyVisual = $item->divideImages($currentItem->item_image)[0] ?>
              <img src=<?= ASSETS . "images/" . $keyVisual ?> alt="商品画像">
            </div>
            <h4>
              <p class="c-item__worksName"><?= $currentItem->anime_title ?></p>
              <p class="c-item__name"><?= $currentItem->item_name ?></p>
            </h4>
            <p class="c-item__plice">&yen;<span class="js-subtotal"><?= (int)$currentItem->item_price * (int)$currentItem->item_quantity ?></span></p>
          </a>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>

  <div class="c-itemList__amount">
    <p>合計(税抜)<span class="js-total"></span></p>
    <p>発送予定日:<?= date("n/d", strtotime("+3 day")) ?> </p>
  </div>
  <div class="c-itemList__buttons">
    <a href=<?= URL::route("") ?>>
      <button class="c-btn--primary">トップへ戻る</button>
    </a>
    <a href=<?= URL::route("mypage/") ?>>
      <button class="c-btn--primary">マイページへ</button>
    </a>
  </div>
  <div class="c-completion__recommend">
    <p class="c-completion__recommendTxt">
      おっと、こちらも購入されています。
    </p>

    <ul class="c-completion__recommendList">
      <?php foreach ($random_recommend_items as $currentItem) : ?>
        <li class="c-itemList__list">
          <div class="c-item">
            <a href=<?= URL::route("goods/goods_id={$currentItem->item_id}") ?>>
              <div class="c-item__image">
                <?php $keyVisual = $item->divideImages($currentItem->item_image)[0] ?>
                <img src=<?= ASSETS . "images/" . $keyVisual ?> alt="商品画像">
              </div>
              <h4>
                <p class="c-item__worksName"><?= $currentItem->anime_title ?></p>
                <p class="c-item__name"><?= $currentItem->item_name ?></p>
              </h4>
              <p class="c-item__plice">&yen;<?= $currentItem->item_price ?></p>
            </a>
          </div>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>

<script>
  const subTotalArray = document.querySelectorAll(".js-subtotal");
  const resultDOM = document.querySelector(".js-total");
  let total = 0;
  subTotalArray.forEach(value => {
    total += parseInt(value.textContent);
  })

  resultDOM.textContent = `¥${total}`;
</script>