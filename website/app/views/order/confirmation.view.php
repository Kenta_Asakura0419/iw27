<div class="c-confirmation">
  <h2 class="c-sectionTitle">購入確認</h2>

  <ul class="c-confirmation__list">
    <?php foreach ($stockItems as  $currentItem) : ?>
      <li class="c-confirmation__listContainer">

        <!-- <div> -->
        <?php $keyVisual = $item->divideImages($currentItem->item_image)[0] ?>
        <img src=<?= ASSETS . "images/" . $keyVisual ?> alt="商品画像" class="c-confirmation__image">

        <div class="c-confirmation__itemInfo">

          <div>
            <p class="c-confirmation__worksTitle">
              <?= $currentItem->anime_title ?>
            </p>

            <p class="c-confirmation__itemName">
              <?= $currentItem->item_name ?>
            </p>
          </div>

          <div>

            <div class="c-confirmation__breakdown">
              <p class="c-confirmation__number">
                数量: <?= $currentItem->item_quantity ?>
              </p>

              <p class="c-confirmation__subtotal">&yen;<span><?= $currentItem->item_price * $currentItem->item_quantity ?></span>(税抜)</p>
            </div>

            <a href=<?= URL::route("order/?c=" . urlencode($cart_id_hash) . "&item_id={$currentItem->item_id}&delete=true") ?>>
              <button class="c-confirmation__delete">削除</button>
            </a>

          </div>

          <!-- </div> -->

        </div>
      </li>
    <?php endforeach; ?>
  </ul>

  <p class="c-confirmation__amount">
    合計(税抜)<span></span>
  </p>

  <div class="c-selectArea">
    <h3 class="c-sectionTitle">お届け先</h3>
    <ul class="c-selectArea__container">
      <li class="c-selectArea__list">
        <label for="adress">
          <input type="radio" id="adress" name="adress" class="c-selectArea__radio" checked=" checked">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div>
            <p>〒201-0014</p>
            <p>東京都狛江市東和泉一丁目28番19号　クリームハイツ　203号室</p>
            <p>朝倉健太</p>
          </div>
        </label>
      </li>
      <li class="c-selectArea__list">
        <label for="newAdress">
          <input type="radio" id="newAdress" name="adress" class="c-selectArea__radio">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div>
            <p>新しい住所を追加する</p>
          </div>
        </label>
        <div class="c-inputArea">
          <p>お名前</p>
          <div class="c-inputArea__inputField c-inputArea__name">
            <div>
              <p>性</p>
              <input type="text" name="" id="" class="c-inputArea__seimei">
            </div>
            <div>
              <p>名</p>
              <input type="text" name="" id="" class="c-inputArea__seimei">
            </div>
          </div>
          <p>カナ</p>
          <div class="c-inputArea__inputField c-inputArea__name">
            <div>
              <p>セイ</p>
              <input type="text" name="" id="" class="c-inputArea__seimei">
            </div>
            <div>
              <p>メイ</p>
              <input type="text" name="" id="" class="c-inputArea__seimei">
            </div>
          </div>
          <p>電話番号</p>
          <div class="c-inputArea__inputField">
            <input type="tel" class="c-inputArea__tel">
          </div>
          <p>郵便番号</p>
          <div class="c-inputArea__inputField">
            <input type="tel" class="c-inputArea__zipcode">
            -
            <input type="tel" class="c-inputArea__zipcode">
          </div>
          <p>都道府県</p>
          <div class="c-inputArea__inputField">
            <select name="pref_name" class="c-inputArea__prefectures">
              <option value="" selected>選択してください</option>
              <option value="北海道">北海道</option>
              <option value="青森県">青森県</option>
              <option value="岩手県">岩手県</option>
              <option value="宮城県">宮城県</option>
              <option value="秋田県">秋田県</option>
              <option value="山形県">山形県</option>
              <option value="福島県">福島県</option>
              <option value="茨城県">茨城県</option>
              <option value="栃木県">栃木県</option>
              <option value="群馬県">群馬県</option>
              <option value="埼玉県">埼玉県</option>
              <option value="千葉県">千葉県</option>
              <option value="東京都">東京都</option>
              <option value="神奈川県">神奈川県</option>
              <option value="新潟県">新潟県</option>
              <option value="富山県">富山県</option>
              <option value="石川県">石川県</option>
              <option value="福井県">福井県</option>
              <option value="山梨県">山梨県</option>
              <option value="長野県">長野県</option>
              <option value="岐阜県">岐阜県</option>
              <option value="静岡県">静岡県</option>
              <option value="愛知県">愛知県</option>
              <option value="三重県">三重県</option>
              <option value="滋賀県">滋賀県</option>
              <option value="京都府">京都府</option>
              <option value="大阪府">大阪府</option>
              <option value="兵庫県">兵庫県</option>
              <option value="奈良県">奈良県</option>
              <option value="和歌山県">和歌山県</option>
              <option value="鳥取県">鳥取県</option>
              <option value="島根県">島根県</option>
              <option value="岡山県">岡山県</option>
              <option value="広島県">広島県</option>
              <option value="山口県">山口県</option>
              <option value="徳島県">徳島県</option>
              <option value="香川県">香川県</option>
              <option value="愛媛県">愛媛県</option>
              <option value="高知県">高知県</option>
              <option value="福岡県">福岡県</option>
              <option value="佐賀県">佐賀県</option>
              <option value="長崎県">長崎県</option>
              <option value="熊本県">熊本県</option>
              <option value="大分県">大分県</option>
              <option value="宮崎県">宮崎県</option>
              <option value="鹿児島県">鹿児島県</option>
              <option value="沖縄県">沖縄県</option>
            </select>
          </div>
          <p>市区町村</p>
          <div class="c-inputArea__inputField">
            <input type="text" class="c-inputArea__adress">
          </div>
          <p>番地</p>
          <div class="c-inputArea__inputField">
            <input type="text" class="c-inputArea__adress">
          </div>
          <p>マンション名・部屋番号</p>
          <div class="c-inputArea__inputField">
            <input type="text" class="c-inputArea__adress">
          </div>

        </div>
      </li>
    </ul>
  </div>

  <div class="c-selectArea">
    <h3 class="c-sectionTitle">お支払い情報</h3>
    <ul class="c-selectArea__container">
      <li class="c-selectArea__list">
        <label for="cache1">
          <input type="radio" id="cache1" name="cache" checked=" checked" class="c-selectArea__radio">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div class="c-selectArea__areaName">
            <p>代引き引換<span>＊手数料215円がかかります</span></p>
          </div>
        </label>
      </li>
      <li class="c-selectArea__list">
        <label for="cache2">
          <input type="radio" id="cache2" name="cache" class="c-selectArea__radio">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div class="c-selectArea__areaName">
            <p>クレジットカード</p>
          </div>
        </label>
        <div class="c-inputArea__cashCard">

          <label>
            <p>署名</p>
            <div class="c-inputArea__inputField">
              <input type="text" class="c-inputArea__cardName">
            </div>
          </label>

          <label>
            <p>カード番号</p>
            <div class="c-inputArea__inputField">
              <input type="text" class="c-inputArea__cardId">
              -
              <input type="text" class="c-inputArea__cardId">
              -
              <input type="text" class="c-inputArea__cardId">
            </div>
          </label>

          <label>
            <p>有効期限</p>
            <div class="c-inputArea__inputField">
              <input type="tel" class="c-inputArea__exp">
              /
              <input type="tel" class="c-inputArea__exp">
            </div>
          </label>

          <label>
            <p>CVC</p>
            <div class="c-inputArea__inputField">
              <input type="tel" class="c-inputArea__cvc">
            </div>
          </label>
        </div>
      </li>
      <li class="c-selectArea__list">
        <label for="cache3">
          <input type="radio" id="cache3" name="cache" class="c-selectArea__radio">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div>
            <p>paypal　※確定ボタン押下後、決済画面に遷移します。</p>
          </div>
        </label>
      </li>
      <li class="c-selectArea__list">
        <label for="cache4">
          <input type="radio" id="cache4" name="cache" class="c-selectArea__radio">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div>
            <p>LINE pay　※確定ボタン押下後、決済画面に遷移します。</p>
          </div>
        </label>
      </li>
      <li class="c-selectArea__list">
        <label for="cache5">
          <input type="radio" id="cache5" name="cache" class="c-selectArea__radio">
          <div class="c-selectArea__button">
            <button></button>
          </div>
          <div>
            <p>メルペイ　※確定ボタン押下後、決済画面に遷移します。</p>
          </div>
        </label>
      </li>
    </ul>
  </div>

  <form action=<?= URL::route("order/done.php") ?> method="POST">
    <input type="hidden" name="c" value=<?= $cart_id_hash ?>>
    <div class="c-btn__submit">
      <button>購入を確定する</button>
    </div>
  </form>
</div>

<script>
  const subTotalArray = document.querySelectorAll(".c-confirmation__subtotal span");
  const resultDOM = document.querySelector(".c-confirmation__amount span");
  let total = 0;
  subTotalArray.forEach(value => {
    total += parseInt(value.textContent);
  })

  resultDOM.textContent = `¥${total}`;
</script>