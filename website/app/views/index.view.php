<?php

/** @var /Item $item */
?>

<div class="c-slider js-swiper-container">
  <div class="swiper-wrapper js-swiper">
    <div class="swiper-slide"><img src=<?= ASSETS . "images/banners/1.jpeg" ?> alt="" class="c-siler__image"></div>
    <div class="swiper-slide"><img src=<?= ASSETS . "images/banners/13831934955ab06f6397de00011.jpeg" ?> alt="" class="c-siler__image"></div>
    <div class="swiper-slide"><img src=<?= ASSETS . "images/banners/E38390E3838AE383BC1.jpeg" ?> alt="" class="c-siler__image"></div>
    <div class="swiper-slide"><img src=<?= ASSETS . "images/banners/FAldpfLVkAIa20P.jpeg" ?> alt="" class="c-siler__image"></div>
  </div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
</div>
<div class="c-news">
  <h3 class="c-news__title">新着情報</h3>
  <ul>
    <li class="c-news__newsList">
      <a>現在の出荷状況について(2022/02/25更新)</a>
    </li>
    <li class="c-news__newsList">
      <a>きららストアオープン！！！</a>
    </li>
  </ul>
</div>
<div class="c-itemListWrapper">
  <div>
    <!-- <section class="c-category">
      <h3 class="c-category_title">作品一覧</h3>
      <ul class="c-category_container">
        <?php foreach ($animes as $anime) : ?>
          <li class="c-category_worksList">
            <a href="<?= URL::route("animes/?anime_id={$anime->anime_id}") ?>">
              <button class="c-category_worksButton">
                <?= $anime->anime_title ?>
              </button>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </section> -->
    <section class="c-category">
      <h3 class="c-sectionTitle">カテゴリ</h3>
      <ul class="c-category_container">
        <?php foreach ($categories as $category) : ?>
          <li class="c-category_list">
            <a href="<?= URL::route("categories/?category_id={$category->category_id}") ?>">
              <button class="c-category_button">
                <?= $category->category_name ?>
              </button>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </section>
  </div>

  <div class="newItemArea c-itemList">
    <h3 class="c-sectionTitle">新着アイテム</h3>
    <ul class="c-itemList__listContainer">
      <?php if (isset($currentItems)) : ?>
        <?php foreach ($currentItems as $cur) : ?>
          <li class="c-itemList__list">
            <section class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$cur->item_id}") ?>>
                <div class="c-item__image">
                  <?php $keyvisual = $item->divideImages($cur->item_image)[0] ?>
                  <img src="<?= ASSETS . "images/{$keyvisual}" ?>" alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $cur->anime_title ?></p>
                  <p class="c-item__name"><?= $cur->item_name ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $cur->item_price ?></p>
              </a>
            </section>
          </li>
        <?php endforeach ?>
      <?php endif ?>
    </ul>
    <div class="c-btn__viewmore">
      <!-- css確認 -->
      <a href=<?= URL::route("goods/") ?>><button>もっと見る</button></a>
    </div>
  </div>

  <div class="populerItemArea c-itemList">
    <h3 class="c-sectionTitle">人気アイテム</h3>
    <ul class="c-itemList__listContainer">
      <?php if (isset($currentItems)) : ?>
        <?php foreach ($currentItems as $cur) : ?>
          <li class="c-itemList__list">
            <section class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$cur->item_id}") ?>>
                <div class="c-item__image">
                  <?php $keyvisual = $item->divideImages($cur->item_image)[0] ?>
                  <img src="<?= ASSETS . "images/{$keyvisual}" ?>" alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $cur->anime_title ?></p>
                  <p class="c-item__name"><?= $cur->item_name ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $cur->item_price ?></p>
              </a>
            </section>
          </li>
        <?php endforeach ?>
      <?php endif ?>
    </ul>
    <div class="c-btn__viewmore">
      <!-- css確認 -->
      <a href=<?= URL::route("goods/") ?>><button>もっと見る</button></a>
    </div>
  </div>


  <div class="populerItemArea c-itemList">
    <h3 class="c-sectionTitle">単行本</h3>
    <ul class="c-itemList__listContainer">
      <?php if (isset($randomBooks)) : ?>
        <?php foreach ($randomBooks as $book) : ?>
          <li class="c-itemList__list">
            <section class="c-item">
              <a href=<?= URL::route("goods/?goods_id={$book->item_id}") ?>>
                <div class="c-item__image c-item__commic">
                  <?php $keyvisual = $item->divideImages($book->item_image)[0] ?>
                  <img src="<?= ASSETS . "images/{$keyvisual}" ?>" alt="商品画像">
                </div>
                <h4>
                  <p class="c-item__worksName"><?= $book->anime_title ?></p>
                  <p class="c-item__name"><?= $book->item_name ?></p>
                </h4>
                <p class="c-item__plice">&yen;<?= $book->item_price ?></p>
              </a>
            </section>
          </li>
        <?php endforeach ?>
      <?php endif ?>
    </ul>
    <div class="c-btn__viewmore">
      <a href=<?= URL::route("categories/?category_id=11") ?>><button>もっと見る</button></a>
    </div>
  </div>
</div>