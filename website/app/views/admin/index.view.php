<div class="l-wrapper">
  <h2 class="c-sectionTitle">管理ページ</h2>
  <ul class="p-admin__controller">
    <li class="c-btn c-btn--primary p-admin__controllerItem"><a href="<?= URL::route('admin/admin_user/') ?>">ユーザー一覧</a></li>
    <li class="c-btn c-btn--primary p-admin__controllerItem"><a href="<?= URL::route('admin/admin_item/') ?>">商品一覧</a></li>
    <li class="c-btn c-btn--primary p-admin__controllerItem"><a href="<?= URL::route('admin/admin_item/add.php') ?>">商品追加</a></li>
  </ul>
</div>