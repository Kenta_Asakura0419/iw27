<div class="l-wrapper">
  <h2 class="c-form__headding">商品更新</h2>
  <form action="update_result.php" method="POST" enctype="multipart/form-data">
    <div class="c-form__content">
      <label class="c-form__label" for="item_name">商品名 <span class="c-form__required">※必須</span></label>
      <input type="text" id="item_name" class="c-form__input" name="item_name" value="<?= @$item->value['item_name'] ?>">
      <p class="c-form__error"><?= @$errors['item_name'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="item_image">商品画像 <span class="c-form__required">※必須</span></label>
      <input type="file" name="item_images[]" id="item_image" class="c-form__input" multiple="multiple">
      <p class="c-form__error"><?= @$errors['item_image'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="anime_title">作品名 <span class="c-form__required">※必須</span></label>
      <input type="text" id="anime_title" class="c-form__input" name="anime_title" value="<?= @$item->value['anime_title'] ?>">
      <p class="c-form__error"><?= @$errors['anime_title'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="category_id">カテゴリ <span class="c-form__required">※必須</span></label>
      <select name="category_id" id="category_id" class="c-form__input">
        <?php foreach ($categories as $key => $category) : ?>
          <option value="<?= $key + 1 ?>" <?= Form::selected($key + 1, $item->value['category_id']) ?>><?= $category->category_name ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="item_price">価格 <span class="c-form__required">※必須</span></label>
      <input type="number" id="item_price" class="c-form__input" name="item_price" value="<?= @$item->value['item_price'] ?>">
      <p class="c-form__error"><?= @$errors['item_price'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="item_stock">在庫数 <span class="c-form__required">※必須</span></label>
      <input type="number" id="item_stock" class="c-form__input" name="item_stock" value="<?= @$item->value['item_stock'] ?>">
      <p class="c-form__error"><?= @$errors['item_stock'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="item_description">説明文 <span class="c-form__required">※必須</span></label>
      <textarea name="item_description" id="item_description" cols="30" rows="10" class="c-form__input"><?= @$item->value['item_description'] ?></textarea>
      <p class="c-form__error"><?= @$errors['item_description'] ?></p>
    </div>

    <input type="hidden" name="item_id" value="<?= $_GET['item_id'] ?>">

    <button class="c-btn c-btn--primary">更新</button>
  </form>
</div>