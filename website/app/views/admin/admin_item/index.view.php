<div class="l-wrapper c-admin">
  <h2>商品一覧</h2>
  <p><?= $count ?>件</p>
  <table class="c-admin__table">
    <tr>
      <th>商品ID</th>
      <th>商品名</th>
      <th>画像ファイル名</th>
      <th>作品名</th>
      <th>カテゴリー</th>
      <th>価格</th>
      <th>在庫数</th>
      <th>説明文</th>
      <th>作成日</th>
      <th>更新日</th>
      <th>操作</th>
    </tr>
    <?php if ($items) : ?>
      <?php foreach ($items as $item) : ?>
        <tr>
          <td><?= $item->item_id ?></td>
          <td><?= $item->item_name ?></td>
          <td><?= $item->item_image ?></td>
          <td><?= $item->anime_title ?></td>
          <td><?= $item->category_name ?></td>
          <td><?= $item->item_price ?></td>
          <td><?= $item->item_stock ?></td>
          <td><?= $item->item_description ?></td>
          <td><?= $item->created_at ?></td>
          <td><?= $item->updated_at ?></td>
          <td>
            <a href=<?= URL::route("admin/admin_item/update.php?item_id={$item->item_id}") ?> class="c-admin__btn c-admin__btn--update">更新</a>
            <a href=<?= URL::route("admin/admin_item/delete.php?item_id={$item->item_id}") ?> class="c-admin__btn c-admin__btn--delete">削除</a>
          </td>
        </tr>
      <?php endforeach ?>
    <?php endif ?>
  </table>

  <?php include('app/views/components/paginate.php') ?>
</div>