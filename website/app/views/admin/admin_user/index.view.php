<div class="l-wrapper c-admin">
  <h2>ユーザ一覧</h2>
  <p><?= $count ?>件</p>
  <table class="c-admin__table">
    <tr>
      <th>ID</th>
      <th>氏名</th>
      <th>メールアドレス</th>
      <th>作成日</th>
      <th>更新日</th>
    </tr>
    <?php if ($users) : ?>
      <?php foreach ($users as $user) : ?>
        <tr>
          <td><?= $user->id ?></td>
          <td><?= $user->name ?></td>
          <td><?= $user->email ?></td>
          <td><?= $user->created_at ?></td>
          <td><?= $user->updated_at ?></td>
        </tr>
      <?php endforeach ?>
    <?php else : ?>
      <p class="c-form__error">Not Found users.</p>
    <?php endif ?>
  </table>

  <?php include('app/views/components/paginate.php') ?>
</div>