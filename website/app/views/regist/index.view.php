<div class="c-form__wrapper">
  <h2 class="c-form__headding">会員登録</h2>
  <form action="confirm.php" method="POST">
    <div class="c-form__content">
      <label class="c-form__label" for="name">ユーザー名 <span class="c-form__required">※必須</span></label>
      <input type="text" id="name" class="c-form__input" name="name" value="<?= @$user->value['name'] ?>">
      <p class="c-form__error"><?= @$errors['name'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="email">メールアドレス <span class="c-form__required">※必須</span></label>
      <input type="email" id="email" class="c-form__input" name="email" value="<?= @$user->value['email'] ?>">
      <p class="c-form__error"><?= @$errors['email'] ?></p>
    </div>

    <div class="c-form__content">
      <label class="c-form__label" for="password">パスワード <span class="c-form__required">※必須</span></label>
      <input type="password" id="password" class="c-form__input js-eye__input" name="password">
      <ion-icon class="c-form__eye js-eye" name="eye-off-outline"></ion-icon>
      <p class="c-form__error"><?= @$errors['password'] ?></p>
    </div>

    <button class="c-btn c-btn--primary u-w100">確認画面へ</button>
  </form>
</div>