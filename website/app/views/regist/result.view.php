<div class="c-form__wrapper p-result">
  <h2 class="c-form__headding">会員登録完了</h2>
  <a href="<?= URL::route('login/') ?>" class="c-btn c-btn--primary">ログイン画面へ</a>
</div>