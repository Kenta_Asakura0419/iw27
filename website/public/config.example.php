<?php
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__DIR__));

require_once 'helpers/Form.php';
require_once 'helpers/Session.php';
require_once 'helpers/URL.php';

// DB設定
define('DB_CONNECTION', 'mysql');
define('DB_PORT', '3306');
define('DB_DATABASE', 'LAA1202786-admin');
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');


define('BASE_URL', '/iw27/website/');
define('PAGES', BASE_URL . 'public/');
define('ASSETS', BASE_URL . 'assets/');
