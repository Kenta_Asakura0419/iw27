var swiper = new Swiper('.js-swiper-container', {
  loop: true,
  effect: 'fade',
  slidesPerView: 1,
  navigation: false,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
