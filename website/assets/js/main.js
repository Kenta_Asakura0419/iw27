window.addEventListener('DOMContentLoaded', () => {
  class ToggleForm {
    constructor(btn, input) {
      this.btn = document.querySelectorAll(btn);
      this.input = document.querySelectorAll(input);
      this.toggle();
    }

    toggle() {
      this.btn &&
        this.btn.forEach((btn) => {
          btn.addEventListener('click', () => {
            const attr = btn.getAttribute('name');

            if (attr === 'eye-off-outline') {
              btn.setAttribute('name', 'eye-outline');
              btn.previousElementSibling.type = 'text';
            } else {
              btn.setAttribute('name', 'eye-off-outline');
              btn.previousElementSibling.type = 'password';
            }
          });
        });
    }
  }

  const toggleForm = new ToggleForm('.js-eye', '.js-eye__input');

  const humbergerButton = document.querySelector('.js-humbergerButton');
  const humberger = document.querySelector('.js-humberger');

  humbergerButton.addEventListener('click', () => {
    humbergerButton.classList.toggle('active');
    humberger.classList.toggle('active');
  });

  //商品詳細の＋ーボタン
  const goodsQuantity = document.querySelector('.js-goodsQuantity');
  const quantityButton = document.querySelectorAll('.js-quantityButton');
  let n = goodsQuantity.value;

  quantityButton.forEach((button) => {
    button.addEventListener('click', (e) => {
      const plusButton = e.target.classList.contains('js-plusButton');
      plusButton ? n++ : n--;
      if (n <= -1) {
        n = 0;
      }
      goodsQuantity.value = n;
    });
  });
});
