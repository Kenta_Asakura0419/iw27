<?php
class URL
{
  public static function route($path)
  {
    $url = PAGES . $path;
    return htmlspecialchars($url, ENT_QUOTES, "UTF-8");
  }

  public static function redirect($path)
  {
    header("Location: {$path}");
    exit;
  }
}
