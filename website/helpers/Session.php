<?php
session_start();
session_regenerate_id(true);

class Session
{

  //セッション読み込み
  public static function load($key)
  {
    if (isset($_SESSION[$key])) {
      return $_SESSION[$key];
    }
  }

  //セッション保存
  public static function save($key, $value)
  {
    $_SESSION[$key] = $value;
  }

  //セッションクリア
  public static function clear($key)
  {
    if (isset($_SESSION[$key])) unset($_SESSION[$key]);
  }

  // キーがあるか
  public static function has($key)
  {
    return array_key_exists($key, $_SESSION);
  }
}
