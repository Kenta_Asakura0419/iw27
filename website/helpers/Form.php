<?php
class Form
{
  //ラジオボタンの選択
  public static function isInvalid($error)
  {
    if (!empty($error)) {
      return 'is-invalid';
    }
  }

  //ラジオボタンの選択
  public static function checked($value, $target)
  {
    if ($value == $target) {
      return 'checked';
    }
  }

  //プルダウンの選択
  public static function selected($value, $target)
  {
    if ($value == $target) {
      return 'selected';
    }
  }
}
